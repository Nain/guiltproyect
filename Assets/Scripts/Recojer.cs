﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Recojer : MonoBehaviour
{

	const string TOALLA_TAG = "Toalla";
	const string BAÑO_TAG = "Baño";
	const string ROPA_TAG = "ropa";
	public float sightLength;
	public bool debugSight;

	public bool mirando;
	public bool _tomasteToalla;
	public bool _tomasteBaño;
	public bool _tomasteRopa;

	// Use this for initialization
	void Start () {
		_tomasteToalla = false;
		_tomasteRopa = false;
		_tomasteBaño = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		CastSight();
	}

	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.blue;
		Gizmos.DrawLine(Camera.main.transform.position, Camera.main.transform.position + (Camera.main.transform.forward * sightLength)); // alcance para la colicion del raycast
	}

	void CastSight()
	{
		Ray ray = new Ray(Camera.main.transform.position,  (Camera.main.transform.forward * sightLength));
		if(debugSight) Debug.DrawRay(ray.origin, ray.direction * sightLength, Color.blue);

		RaycastHit hits;
		mirando = false;
		if(Physics.Raycast(ray, out hits))
		{
			if(hits.transform.gameObject.tag == TOALLA_TAG && hits.distance <= 2.0f)
			{
				mirando = true;
				if(Input.GetKey("e"))
				{
					_tomasteToalla = true;
					Destroy(hits.transform.gameObject);
				}
			}
			if(hits.transform.gameObject.tag == BAÑO_TAG && hits.distance <= 2.0f)
			{
				mirando = true;
				if(_tomasteToalla == true)
				{
					if(Input.GetKey("e"))
					{
						_tomasteBaño = true;
						//AQUI VA LA PANTALLA NEGRA
					}
				}
			}
			if(hits.transform.gameObject.tag == ROPA_TAG && hits.distance <= 2.0f)
			{
				mirando = true;
				if(_tomasteBaño == true)
				{
					if(Input.GetKey("e"))
					{
						_tomasteRopa = true;
						Destroy(hits.transform.gameObject);
					}
				}
			}
		}		
	}

	void OnGUI()
	{
		if(mirando == true) 
		{
			GUI.Label(new Rect(350, 350, 100, 30), "Interactuar");
		}
	}

}
